package com.kuma4zen.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface GenerateBuilder {
    Class<?> value();
}
