# annotation-processor-example

## create custom annotation
for example:
```java
public @interface GenerateBuilder {
    Class<?> value();
}
```

## create processor that process the annotation on compile
```java
@SupportedAnnotationTypes({"com.kuma4zen.annotation.GenerateBuilder"})
public class GenerateBuilderProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        /* todo */
        return true;
    }
}
```
for example, we want to use the annotation on an interface
```java
roundEnv.getElementsAnnotatedWithAny(Set.of(GenerateBuilder.class)).stream()
        .filter(element -> element.getKind() == ElementKind.INTERFACE)
        .forEach(this::processInterfaceElement);
```

## add velocity to project
maven dependency:
```xml
<dependency>
    <groupId>org.apache.velocity</groupId>
    <artifactId>velocity</artifactId>
</dependency>
```
velocity files:
`velocity.properties` and `javax.annotation.processing.Processor`

load the `velocity.properties`
```java
private VelocityEngine initEngine() throws Exception {
    var props = new Properties();
    var url = this.getClass().getClassLoader().getResource("velocity.properties");
    assert url != null;
    props.load(url.openStream());
    var ve = new VelocityEngine(props);
    ve.init();
    return ve;
}
```

## create velocity template
for example: `builder.vm` (for example we want to generate a builder class)

put all needed information to `VelocityContext` and define the output source name (here variable `qualifiedName`)
```java
private static final String TEMPLATE = "builder.vm";

private void prepareContext(VelocityContext vc) {
    vc.put("packageName", packageName);
    vc.put("imports", imports);
    vc.put("implName", implName);
    vc.put("implTypeName", implTypeName);
    vc.put("implParentName", implParentName);
    vc.put("implTypeIsAbstract", implTypeIsAbstract);
    vc.put("constructors", constructors);
    vc.put("methods", methods);
    vc.put("implCode", implCode);
}

private void generateBuilderClass() {
    if (qualifiedName.isEmpty()) {
        return;
    }

    try {
        var vc = new VelocityContext();
        prepareContext(vc);

        var vt = initEngine().getTemplate(TEMPLATE);
        var jfo = processingEnv.getFiler().createSourceFile(qualifiedName);
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "creating src file: " + jfo.toUri());

        var writer = jfo.openWriter();
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "applying template: " + vt.getName());

        vt.merge(vc, writer);
        writer.close();
    } catch (Exception e) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getLocalizedMessage());
    }
}
```

## add dependency of processor module
(as `provided` to do not pull others dependencies that used only for compile):
```xml
<dependency>
    <artifactId>processor</artifactId>
    <scope>provided</scope>
</dependency>
```

## add custom code to processor
for example, we define here the `packageName` like `.gen` instead of `.annotation`
(we want to use our annotation `GenerateBuilder` on an interface placed by `com.kuma4zen.builder.annotation.*`
and generated classes should be generated to `com.kuma4zen.builder.gen.*`)
```java
var packageName = ((PackageElement) interfaceElement.getEnclosingElement())
        .getQualifiedName().toString().replace(".annotation", ".gen");
```
we define here the suffix for `implName` and an abstract parent class `AbstractBuilder` (that is not generated)
```java
private static final String BUILDER = "Builder";
private static final String ABSTRACT_BUILDER = "com.kuma4zen.builder.AbstractBuilder";
```
we define the `implName`
```java
implName = typeClass.getSimpleName() + BUILDER;
```
we define the `qualifiedName`
```java
qualifiedName = packageName + "." + implName;
```
we define the `implParentName`
```java
var superClass = typeClass.getSuperclass();
if (superClass.equals(Object.class)) {
    implParentName = ABSTRACT_BUILDER;
} else {
    implParentName = superClass.getSimpleName() + BUILDER;
}
```
we search for non `Deprecated` constructors
```java
        .filter(constructor -> !constructor.isAnnotationPresent(Deprecated.class))
```
and non `Deprecated`, `public`, `void` `add` and `set` methods
```java
        .filter(method -> method.getReturnType().equals(void.class))
        .filter(method -> method.getName().startsWith("add") || method.getName().startsWith("set"))
        .filter(method -> Modifier.isPublic(method.getModifiers()))
        .filter(method -> !method.isAnnotationPresent(Deprecated.class))
```

## generate custom Builder
add dependencies to builder module (as `optional` to do not pull dependencies that may not be necessary):
```xml
<optional>true</optional>
```
use `@GenerateBuilder` annotation on an interface (interface name is not important) to generate custom Builder class,
as example (generate `SpatialBuilder` for `Spatial`):
```java
@GenerateBuilder(Spatial.class)
public interface ISpatial {
}
```

## usage
add dependency of builder module
`||` use the compiled jar
`||` copy generated classes

## enjoy
