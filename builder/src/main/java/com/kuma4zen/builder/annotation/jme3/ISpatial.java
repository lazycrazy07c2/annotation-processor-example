package com.kuma4zen.builder.annotation.jme3;

import com.jme3.scene.Spatial;
import com.kuma4zen.annotation.GenerateBuilder;

@GenerateBuilder(Spatial.class)
public interface ISpatial {
}
